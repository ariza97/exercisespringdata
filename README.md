# ExerciseSpringData
=============

Ejercicio elaborado durante la clase del 23 de agosto, en el cual se hace uso de Spring Data; el proyecto **ExerciseSpringData** cuenta con dos ramas de desarrollo ***mongo*** y ***h2***, además de la ***master***, como el nombre de cada rama lo puede indicar se hace uso de cada uno de estos de tipos de base de datos para probar su integración con un proyecto Spring.
La descripción de cada rama se encuentra en su README correspondiente.